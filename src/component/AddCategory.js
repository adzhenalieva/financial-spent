import React from "react";

const CategoryForm = props => (
    <form>
        <p>
            <input className="categoryInput" type="text" value = {props.value}  onChange = {props.changeValue} />
            <input className="categoryInput" type="number" value = {props.cost}  onChange = {props.changeCost} /> KGS
        </p>
        <button className="btnAdd" onClick = {props.onClick}>Add</button>
        <p className="TotalCost">Total Spent   {props.total}   KGS</p>
    </form>

);
export default CategoryForm;