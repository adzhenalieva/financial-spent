import React from "react";

const AddedCategory = props => (
    <div className="categoryDiv">
        <span className="AddedCategory">{props.text}</span>
        <span className="AddedCategory">{props.cost} KGS</span>
        <button className="btnDelete" onClick = {props.remove}>x</button>
    </div>
);
export default AddedCategory;