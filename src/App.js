import React, { Component } from 'react';
import './App.css';
import CategoryForm from "./component/AddCategory";
import AddedCategory from "./component/AddedCategory";

class App extends Component {
  state = {
    value: "Add new category",
    cost: 0,
    categories: [],
    totalCost: 0
  };

  changeCategory = (event) => {
    this.setState({value: event.target.value});
  };
  changeCost = (event) => {
    this.setState({cost: parseInt(event.target.value)});
  };

  addCategory = (event) => {
    if(this.state.value !== " " && this.state.value !== "Add new category" && this.state.cost !== ' ' && this.state.cost !== 0 && this.state.cost > 0 ){
      const categoryArray = [...this.state.categories];
      event.preventDefault();

      let newCategory = {};
      newCategory.text = this.state.value;
      newCategory.cost = this.state.cost;
      categoryArray.push(newCategory);
      let totalCost = this.state.totalCost + parseInt(newCategory.cost);

      this.setState({value: ' ', cost: ' ', categories: categoryArray, totalCost})
    } else {
      event.preventDefault();
      alert("Please, enter a category or cost. Cost should be more than 0")
    }
  };

  removeTask = (id) => {
    const allCategories = [...this.state.categories];
    let totalCost = this.state.totalCost;
    totalCost = totalCost - allCategories[id].cost;
    allCategories.splice(id, 1);
    this.setState({categories: allCategories, totalCost});
  };

  render() {
    let categories = this.state.categories.map((category, id) => (
        <AddedCategory
            key = {id}
            text = {category.text}
            cost = {category.cost}
            remove = {() => this.removeTask(id)}
        >
        </AddedCategory>
    ));
    return (
        <div className="App">
          <p>Financial spent app</p>
          <CategoryForm value = {this.state.value}
                        cost = {this.state.cost}
                        total = {this.state.totalCost}
                        changeValue = {event => this.changeCategory(event)}
                        changeCost = {event => this.changeCost(event)}
                        onClick = {event => this.addCategory(event)}
          >
          </CategoryForm>
          {categories}
        </div>
    );
  }
}

export default App;
